﻿
using ResultPattern;

PatternResultTValueAndTError();
PatternResultTValue();
PatternResult();

static void PatternResultTValueAndTError()
{
	Result<double, DivideByZeroException> r = Divide(4.5, 7.8);

	if (r)
	{
		Console.WriteLine($"Success: {r.IsSuccess}");
		Console.WriteLine($"Result: {r.Value}");
	}
	else
	{
		Console.WriteLine($"Error: {r.Message}");
	}
}

static void PatternResultTValue()
{
	Result<int> r = IsSix(6);

	if (r)
	{
		Console.WriteLine($"Success: {r.IsSuccess}");
		Console.WriteLine($"Result: {r.Value}");
	}
	else
	{
		Console.WriteLine($"Error: {r.Message}");
	}
}

static void PatternResult()
{
	var r = StringIsEmpty(string.Empty);

	if (r)
	{
		Console.WriteLine($"Success: {r.IsSuccess}");
	}
	else
	{
		Console.WriteLine($"Error: {r.Message}");
	}
}

static Result<double, DivideByZeroException> Divide(double a, double b)
{
	if (b == 0)
	{
		return Result<double, DivideByZeroException>.Failure(new DivideByZeroException());
	}
	return Result<double, DivideByZeroException>.Success(a / b);
}

static Result<int> IsSix(int value)
{
	if(value is 6)
	{
		return Result<int>.Success(value);
	}

	return Result<int>.Failure($"{value} is not 6");
}

static Result StringIsEmpty(string value)
{
	if(value == string.Empty)
	{
		return Result.Success();
	}

	return Result.Failure($"{value} is not empty");
}
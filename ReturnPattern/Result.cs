﻿namespace ResultPattern
{
	/// <summary>
	/// Represents the result of an operation that can either be successful or fail with an error.
	/// </summary>
	/// <typeparam name="TValue">The type of the value in case of success.</typeparam>
	/// <typeparam name="TError">The type of the error in case of failure, must derive from Exception.</typeparam>
	public class Result<TValue, TError> where TError : Exception
	{
		/// <summary>
		/// Gets the value of the result if it is successful.
		/// </summary>
		public TValue? Value { get; }

		/// <summary>
		/// Gets the error of the result if it has failed.
		/// </summary>
		public TError? Error { get; }

		/// <summary>
		/// Indicates whether the result is successful.
		/// </summary>
		public bool IsSuccess => Error is null;

		/// <summary>
		/// Indicates whether the result is a failure.
		/// </summary>
		public bool IsFailure => !IsSuccess;

		/// <summary>
		/// Indicates whether the result has a value (not null).
		/// </summary>
		public bool HasValue => Value is null;

		/// <summary>
		/// Gets the message associated with the result.
		/// </summary>
		public string? Message => Error?.Message ?? string.Empty;

		/// <summary>
		/// Initializes a new instance of the Result class with a successful value.
		/// </summary>
		/// <param name="value">The value of the successful result.</param>
		protected Result(TValue value)
		{
			Value = value;
		}

		/// <summary>
		/// Initializes a new instance of the Result class with an error.
		/// </summary>
		/// <param name="error">The error of the failed result.</param>
		protected Result(TError error)
		{
			Error = error;
		}

		/// <summary>
		/// Initializes a new instance of the Result class with an error and a default value.
		/// </summary>
		/// <param name="error">The error of a failed result.</param>
		/// <param name="defaultValue">The default value in case of an error.</param>
		protected Result(TError error, TValue defaultValue)
		{
			Error = error;
			Value = defaultValue;
		}

		/// <summary>
		/// Creates a successful result with the specified value.
		/// </summary>
		/// <param name="value">The value of the successful result.</param>
		/// <returns>A successful result.</returns>
		public static Result<TValue, TError> Success(TValue value)
		{
			return new Result<TValue, TError>(value);
		}

		/// <summary>
		/// Creates a failed result with the specified error.
		/// </summary>
		/// <param name="error">The error of the failed result.</param>
		/// <returns>A failed result.</returns>
		public static Result<TValue, TError> Failure(TError error)
		{
			return new Result<TValue, TError>(error);
		}

		/// <summary>
		/// Creates a failed result with the specified error and a default value.
		/// </summary>
		/// <param name="error">The error of a failed result.</param>
		/// <param name="defaultValue">The default value in case of an error.</param>
		/// <returns>A failed result.</returns>
		public static Result<TValue, TError> Failure(TError error, TValue defaultValue)
		{
			return new Result<TValue, TError>(error, defaultValue);
		}

		/// <summary>
		/// Implicitly converts a value to a successful result.
		/// </summary>
		/// <param name="value">The value to be converted.</param>
		public static implicit operator Result<TValue, TError>(TValue value)
		{
			return Success(value);
		}

		/// <summary>
		/// Implicitly converts an error to a failed result.
		/// </summary>
		/// <param name="error">The error to be converted.</param>
		public static implicit operator Result<TValue, TError>(TError error)
		{
			return Failure(error);
		}

		/// <summary>
		/// Implicitly converts a result to a boolean indicating success or failure.
		/// </summary>
		/// <param name="result">The result to be converted.</param>
		public static implicit operator bool(Result<TValue, TError> result)
		{
			return result.IsSuccess;
		}
	}

	/// <summary>
	/// Represents the result of an operation that can either be successful or fail with a string message.
	/// </summary>
	/// <typeparam name="TValue">The type of the value in case of success.</typeparam>
	public class Result<TValue> : Result<TValue, Exception>
	{
		/// <summary>
		/// Initializes a new instance of the Result class with a successful value.
		/// </summary>
		/// <param name="value">The value of the successful result.</param>
		protected Result(TValue value) : base(value)
		{
		}

		/// <summary>
		/// Initializes a new instance of the Result class with an error message.
		/// </summary>
		/// <param name="message">The error message of the failed result.</param>
		protected Result(string message) : base(new Exception(message))
		{
		}

		/// <summary>
		/// Initializes a new instance of the Result class with an error message and a default value.
		/// </summary>
		/// <param name="message">The error message of a failed result.</param>
		/// <param name="defaultValue">The default value in case of an error.</param>
		protected Result(string message, TValue defaultValue) : base(new Exception(message), defaultValue)
		{
		}

		/// <summary>
		/// Creates a successful result with the specified value.
		/// </summary>
		/// <param name="value">The value of the successful result.</param>
		/// <returns>A successful result.</returns>
		public static new Result<TValue> Success(TValue value)
		{
			return new Result<TValue>(value);
		}

		/// <summary>
		/// Creates a failed result with the specified error message.
		/// </summary>
		/// <param name="message">The error message of the failed result.</param>
		/// <returns>A failed result.</returns>
		public static Result<TValue> Failure(string message)
		{
			return new Result<TValue>(message);
		}

		/// <summary>
		/// Creates a failed result with the specified error message and a default value.
		/// </summary>
		/// <param name="message">The error message of a failed result.</param>
		/// <param name="defaultValue">The default value in case of an error.</param>
		/// <returns>A failed result.</returns>
		public static Result<TValue> Failure(string message, TValue defaulValue)
		{
			return new Result<TValue>(message, defaulValue);
		}
	}

	/// <summary>
	/// Represents the result of an operation that can either be successful or fail with a string message,
	/// and the value type is object.
	/// </summary>
	public class Result : Result<object>
	{
		/// <summary>
		/// Initializes a new instance of the Result class with a successful value.
		/// </summary>
		/// <param name="obj">The value of the successful result.</param>
		protected Result(object obj) : base(obj)
		{
		}

		/// <summary>
		/// Initializes a new instance of the Result class with an error message.
		/// </summary>
		/// <param name="message">The error message of the failed result.</param>
		protected Result(string message) : base(message)
		{
		}

		/// <summary>
		/// Initializes a new instance of the Result class with an error message and a default value.
		/// </summary>
		/// <param name="message">The error message of a failed result.</param>
		/// <param name="defaultValue">The default value in case of an error.</param>
		protected Result(string message, object defaultValue) : base(message, defaultValue)
		{
		}

		/// <summary>
		/// Creates a failed result with the specified error message.
		/// </summary>
		/// <param name="message">The error message of the failed result.</param>
		/// <returns>A failed result.</returns>
		public static new Result Failure(string message)
		{
			return new Result(message);
		}

		/// <summary>
		/// Creates a failed result with the specified error message and a default value.
		/// </summary>
		/// <param name="message">The error message of a failed result.</param>
		/// <param name="defaultValue">The default value in case of an error.</param>
		/// <returns>A failed result.</returns>
		public static new Result Failure(string message, object defaultValue)
		{
			return new Result(message, defaultValue);
		}

		/// <summary>
		/// Creates a failed result with the specified error message and value.
		/// </summary>
		/// <typeparam name="TValue">The type of the value in the result.</typeparam>
		/// <param name="message">The error message.</param>
		/// <param name="value">The value associated with the failed result.</param>
		/// <returns>A failed result containing the specified message and value.</returns>
		public static Result<TValue> Failure<TValue>(string message, TValue value)
		{
			return Result<TValue>.Failure(message, value);
		}

		/// <summary>
		/// Creates a failed result with the specified error and value.
		/// </summary>
		/// <typeparam name="TValue">The type of the value in the result.</typeparam>
		/// <typeparam name="TError">The type of the error, must inherit from Exception.</typeparam>
		/// <param name="error">The error associated with the failed result.</param>
		/// <param name="value">The value associated with the failed result.</param>
		/// <returns>A failed result containing the specified error and value.</returns>
		public static Result<TValue, TError> Failure<TValue, TError>(TError error, TValue value) where TError : Exception
		{
			return Result<TValue, TError>.Failure(error, value);
		}

		/// <summary>
		/// Creates a failed result with the specified exception and value.
		/// </summary>
		/// <typeparam name="TValue">The type of the value in the result.</typeparam>
		/// <param name="error">The exception associated with the failed result.</param>
		/// <param name="value">The value associated with the failed result.</param>
		/// <returns>A failed result containing the specified exception and value.</returns>
		public static Result<TValue, Exception> Failure<TValue>(Exception error, TValue value)
		{
			return Result<TValue>.Failure(error, value);
		}

		/// <summary>
		/// Creates a successful result with the specified value.
		/// </summary>
		/// <param name="value">The value of the successful result.</param>
		/// <returns>A successful result.</returns>
		public static new Result Success(object value)
		{
			return new Result(value);
		}

		/// <summary>
		/// Creates a successful result with a true value.
		/// </summary>
		/// <returns>A successful result.</returns>
		public static Result Success()
		{
			return new Result(true);
		}

		/// <summary>
		/// Creates a successful result with the specified value.
		/// </summary>
		/// <typeparam name="TValue">The type of the value in the result.</typeparam>
		/// <param name="value">The value associated with the successful result.</param>
		/// <returns>A successful result containing the specified value.</returns>
		public static Result<TValue> Success<TValue>(TValue value)
		{
			return Result<TValue>.Success(value);
		}

		/// <summary>
		/// Creates a successful result with the specified value.
		/// </summary>
		/// <typeparam name="TValue">The type of the value in the result.</typeparam>
		/// <typeparam name="TError">The type of the error, must inherit from Exception.</typeparam>
		/// <param name="value">The value associated with the successful result.</param>
		/// <returns>A successful result containing the specified value.</returns>
		public static Result<TValue, TError> Success<TValue, TError>(TValue value) where TError : Exception
		{
			return Result<TValue, TError>.Success(value);
		}
	}
}
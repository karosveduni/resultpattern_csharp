# Result Pattern

**This library is designed to avoid throwing exceptions in the code. Instead, it returns a result from the method and describes the error. The library helps reduce the number of try-catch blocks in the code and is less taxing on the program compared to catching and rethrowing exceptions.**

# ResultPattern Code Updates

## Overview of Changes

### Added Comments

- Enhanced code readability and maintainability by adding comments that describe the purpose and functionality of various classes, properties, and methods.

### New Properties

- **`HasValue` Property**: Added to the `Result<TValue, TError>` class. This property indicates whether the result has a value (is not null).

### New Methods

- **`Success` Method**: Added to the `Result` class. This method creates a successful result with a null value, providing a convenient way to signify success without an associated value.

# Program Version 1.2.0

## Overview

Version 1.2.0 of the program includes several improvements and new features. This update focuses on enhancing functionality by adding new methods and fixing existing issues. Below are the detailed changes.

## New Features

### Added New Methods

1. **Failure\<TValue\>(string message, TValue value)**
    - Creates a failed result with the specified error message and value.
    - **Parameters:**
        - `message`: The error message.
        - `value`: The value associated with the failed result.

2. **Failure\<TValue, TError\>(TError error, TValue value)**
    - Creates a failed result with the specified error and value.
    - **Parameters:**
        - `error`: The error associated with the failed result.
        - `value`: The value associated with the failed result.

3. **Failure\<TValue\>(Exception error, TValue value)**
    - Creates a failed result with the specified exception and value.
    - **Parameters:**
        - `error`: The exception associated with the failed result.
        - `value`: The value associated with the failed result.

4. **Success\<TValue\>(TValue value)**
    - Creates a successful result with the specified value.
    - **Parameters:**
        - `value`: The value associated with the successful result.

5. **Success\<TValue, TError\>(TValue value)**
    - Creates a successful result with the specified value.
    - **Parameters:**
        - `value`: The value associated with the successful result.

## Improved Documentation

- Added comprehensive comments to methods and properties to improve code readability and maintainability.

## Example Usage

### Setting a Value

```csharp

using ResultPattern;

PatternResultTValueAndTError();
PatternResultTValue();
PatternResult();

static void PatternResultTValueAndTError()
{
	Result<double, DivideByZeroException> r = Divide(4.5, 7.8);

	if (r)
	{
		Console.WriteLine($"Success: {r.IsSuccess}");
		Console.WriteLine($"Result: {r.Value}");
	}
	else
	{
		Console.WriteLine($"Error: {r.Message}");
	}
}

static void PatternResultTValue()
{
	Result<int> r = IsSix(6);

	if (r)
	{
		Console.WriteLine($"Success: {r.IsSuccess}");
		Console.WriteLine($"Result: {r.Value}");
	}
	else
	{
		Console.WriteLine($"Error: {r.Message}");
	}
}

static void PatternResult()
{
	var r = StringIsEmpty(string.Empty);

	if (r)
	{
		Console.WriteLine($"Success: {r.IsSuccess}");
	}
	else
	{
		Console.WriteLine($"Error: {r.Message}");
	}
}

static Result<double, DivideByZeroException> Divide(double a, double b)
{
	if (b == 0)
	{
		return Result.Failure(new DivideByZeroException(), 0.0);
	}
	return Result.Success<double, DivideByZeroException>(a / b);
}

static Result<int> IsSix(int value)
{
	if(value is 6)
	{
		return Result.Success(value);
	}

	return Result.Failure($"{value} is not 6", 0);
}

static Result StringIsEmpty(string value)
{
	if(value == string.Empty)
	{
		return Result.Success();
	}

	return Result.Failure($"{value} is not empty");
}

```